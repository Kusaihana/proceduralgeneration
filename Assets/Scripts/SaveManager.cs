﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using UniRx;

public class SaveManager : MonoBehaviour
{
    [SerializeField]
    public List<Data> DataList = new List<Data>();

    public Data Data;
    public Data[] PrevData;

    private Player _player;

    public void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    public void LoadChunks()
    {
        foreach (var chunk in PrevData)
        {
            if (!chunk.Name.StartsWith("Block"))
            {
                // Generate chunks
                _player.CreatePlane(chunk.Position, true);
            }
        }
    }

    public void Update(){
		if (Input.GetKeyDown(KeyCode.K)){
            DataList.Clear();
		    foreach (var plane in _player.AllPlanes)
		    {
		        Data = new Data
		        {
		            Position = plane.Key,
                    Name = "Plane@" + plane.Key
		        };
		        DataList.Add(Data);

		        foreach (var child in plane.Value)
		        {
		            Data = new Data
		            {
		                Position = child.Key,
                        Name = "Block@" + child.Key,
                        ParentName = "Plane@" + plane.Key,
                        Type = child.Value.Type,
                        IsDestroyable = child.Value.IsDestroyable
                    };
		            DataList.Add(Data);
		        }
		    }
            var json = JsonHelper.ToJson(DataList.ToArray(), true);
		    StreamWriter writer = new StreamWriter(Application.dataPath + "/Resources/savedgame.json", false);
            
		    writer.WriteLine(json);
		    writer.Close();
		    _player.SaveSuccessText.gameObject.SetActive(true);
		    Observable.Timer(TimeSpan.FromSeconds(3)).Subscribe(x =>
		    {
		        _player.SaveSuccessText.gameObject.SetActive(false);
		    });
		}        
	}
}