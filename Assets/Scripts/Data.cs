﻿using System;
using UnityEngine;

[Serializable]
public class Data {
    public string Name;
    public Vector3 Position;
    public string ParentName;
    public int Type;
    public bool IsDestroyable;
    
}