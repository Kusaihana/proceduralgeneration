﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : MonoBehaviour {

	public int CubeSelection = 0;

	public GameObject ProgressBar;
	public GameObject Plane;
	public GameObject InitialPlane;
    public GameObject ButtonsPanel;
	public List<GameObject> VisiblePlanes = new List<GameObject>();
	public Dictionary<Vector3, Dictionary<Vector3, Properties>> AllPlanes = new Dictionary<Vector3, Dictionary<Vector3, Properties>>();

	public Text SelectedBoxText;
    public Text SaveSuccessText;

	private static Vector3 _lastGeneratedPos = Vector3.zero;

    public void NewWorld(){
        CreatePlane(new Vector3(_lastGeneratedPos.x, 0, _lastGeneratedPos.z));

        CreatePlane(new Vector3(_lastGeneratedPos.x + 40, 0, _lastGeneratedPos.z));
		CreatePlane(new Vector3(_lastGeneratedPos.x - 40, 0, _lastGeneratedPos.z));
		CreatePlane(new Vector3(_lastGeneratedPos.x + 40, 0, _lastGeneratedPos.z - 40));
		CreatePlane(new Vector3(_lastGeneratedPos.x, 0, _lastGeneratedPos.z - 40));
		CreatePlane(new Vector3(_lastGeneratedPos.x - 40, 0, _lastGeneratedPos.z - 40));
		CreatePlane(new Vector3(_lastGeneratedPos.x + 40, 0, _lastGeneratedPos.z + 40));
		CreatePlane(new Vector3(_lastGeneratedPos.x, 0, _lastGeneratedPos.z + 40));
		CreatePlane(new Vector3(_lastGeneratedPos.x - 40, 0, _lastGeneratedPos.z + 40));
	}

    public void Update(){
		if (Input.GetKeyDown(KeyCode.Alpha1)){
			CubeSelection = 0;
			SetSelectionText();
		}
		if (Input.GetKeyDown(KeyCode.Alpha2)){
			CubeSelection = 1;
			SetSelectionText();
		}
		if (Input.GetKeyDown(KeyCode.Alpha3)){
			CubeSelection = 2;
			SetSelectionText();
		}
		if (Input.GetKeyDown(KeyCode.Alpha4)){
			CubeSelection = 3;
			SetSelectionText();
		}
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.GetComponent<FirstPersonController>().enabled = ButtonsPanel.activeSelf;
            ButtonsPanel.SetActive(!ButtonsPanel.activeSelf);
        }

        if (!transform.hasChanged) return;
        if(transform.position.x - _lastGeneratedPos.x > 20){
            DestroyFurtherPlanes();
            var position = new Vector3(_lastGeneratedPos.x + 40, 0, _lastGeneratedPos.z);
            CreatePlane(new Vector3(_lastGeneratedPos.x + 80, 0, _lastGeneratedPos.z));
            CreatePlane(new Vector3(_lastGeneratedPos.x + 80, 0, _lastGeneratedPos.z - 40));
            CreatePlane(new Vector3(_lastGeneratedPos.x + 80, 0, _lastGeneratedPos.z + 40));

            _lastGeneratedPos = position;
        }
        else if(_lastGeneratedPos.x - transform.position.x > 20){
            DestroyFurtherPlanes();
            var position = new Vector3(_lastGeneratedPos.x - 40, 0, _lastGeneratedPos.z);
            CreatePlane(new Vector3(_lastGeneratedPos.x - 80, 0, _lastGeneratedPos.z));
            CreatePlane(new Vector3(_lastGeneratedPos.x - 80, 0, _lastGeneratedPos.z - 40));
            CreatePlane(new Vector3(_lastGeneratedPos.x - 80, 0, _lastGeneratedPos.z + 40));

            _lastGeneratedPos = position;
        }

        if(transform.position.z - _lastGeneratedPos.z > 20){
            DestroyFurtherPlanes();
            var position = new Vector3(_lastGeneratedPos.x, 0, _lastGeneratedPos.z + 40);
            CreatePlane(new Vector3(_lastGeneratedPos.x, 0, _lastGeneratedPos.z + 80));
            CreatePlane(new Vector3(_lastGeneratedPos.x - 40, 0, _lastGeneratedPos.z + 80));
            CreatePlane(new Vector3(_lastGeneratedPos.x + 40, 0, _lastGeneratedPos.z + 80));

            _lastGeneratedPos = position;

        }
        else if(_lastGeneratedPos.z - transform.position.z > 20){
            DestroyFurtherPlanes();
            var position = new Vector3(_lastGeneratedPos.x, 0, _lastGeneratedPos.z - 40);
            CreatePlane(new Vector3(_lastGeneratedPos.x, 0, _lastGeneratedPos.z - 80));
            CreatePlane(new Vector3(_lastGeneratedPos.x - 40, 0, _lastGeneratedPos.z - 80));
            CreatePlane(new Vector3(_lastGeneratedPos.x + 40, 0, _lastGeneratedPos.z - 80));

            _lastGeneratedPos = position;

        }
    }

	public void CreatePlane(Vector3 pos, bool isFromFile = false){
		// check if this plane created before
		var prev = AllPlanes.Where(x => x.Key == pos).ToList();
		if(prev.Count == 1){
			// do not generate random map, load the previous one
			var go = (GameObject)Instantiate((Plane), pos, Quaternion.Euler(90, 0, 0));
			var worldgen = go.GetComponentInChildren<WorldGenerator>();
			worldgen.FirstGeneration = false;
			go.name = "Plane@" + go.transform.position;
			go.transform.position = pos;
			worldgen.PlacedCubes = prev.FirstOrDefault().Value;
			VisiblePlanes.Add(go);
		}else{
			var go = (GameObject)Instantiate((Plane), pos, Quaternion.Euler(90, 0, 0));
		    var worldgen = go.GetComponentInChildren<WorldGenerator>();
            go.name = "Plane@" + go.transform.position;
			go.transform.position = pos;
			VisiblePlanes.Add(go);
			AllPlanes.Add(go.transform.position, go.GetComponentInChildren<WorldGenerator>().PlacedCubes);
		    worldgen.FromFile = isFromFile;
		}
	}

	public void DestroyFurtherPlanes(){
		VisiblePlanes = VisiblePlanes.OrderBy(x => Vector3.Distance(transform.position,x.transform.position)).ToList();
		DestroyLastPlaneFromList();
		DestroyLastPlaneFromList();
		DestroyLastPlaneFromList();
	}

	public void DestroyLastPlaneFromList(){
		var go = VisiblePlanes.ElementAt(VisiblePlanes.Count - 1);
		VisiblePlanes.RemoveAt(VisiblePlanes.Count - 1);
		Destroy(go);
	}

    public void CleanWorld(){
        foreach (var plane in VisiblePlanes){
            Destroy(plane);
        }
        AllPlanes.Clear();
        VisiblePlanes.Clear();
        transform.position = new Vector3(0, 8, 0);
        _lastGeneratedPos = Vector3.zero;
    }

    public void OnQuitGameButtonPressed(){
        Application.Quit();
    }

    private void SetSelectionText(){
		SelectedBoxText.text = "Selected Box: " + (CubeSelection + 1).ToString();
	}

}
