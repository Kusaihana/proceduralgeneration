﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;
using UnityEngine.UI;

public class Block : MonoBehaviour {

	public int Durability = 0;
	public bool IsDestroyable = true;
	public WorldGenerator ParentPlane;

	private Player _player;

	private static readonly List<string> CubeNames = new List<string>();

    public void Awake(){
		CubeNames.Add("Cube1");
		CubeNames.Add("Cube2");
		CubeNames.Add("Cube3");
		CubeNames.Add("Cube4");

		_player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
	}

    public void OnMouseOver()
	{
		if(Input.GetMouseButtonDown(1)){
			var ray = Camera.main.ScreenPointToRay( Input.mousePosition );
			RaycastHit hit;
			if ( Physics.Raycast( ray , out hit , 100 ) )
			{
				var position = hit.transform.position + hit.normal ;
				var rotation =  Quaternion.FromToRotation( Vector3.up , hit.normal );
				CreateCube(position, rotation);
			}
		}
		if(Input.GetMouseButtonDown(0) && IsDestroyable && !_player.ButtonsPanel.activeSelf){
			StartDestroying(Durability);
		}
	}

	public void CreateCube(Vector3 pos, Quaternion rot){
		var go = (GameObject)Instantiate(Resources.Load(CubeNames[_player.CubeSelection]), pos, Quaternion.identity);
		go.name = "Block@" + go.transform.position;
		go.transform.position = pos;
		go.transform.rotation = rot;
		go.GetComponent<Block>().ParentPlane = ParentPlane;
		Properties p;
		p.Type = _player.CubeSelection;
		p.IsDestroyable = true;
		go.transform.parent = ParentPlane.transform;
	    go.GetComponent<Block>().ParentPlane.PlacedCubes.Add(go.transform.position, p);
    }

	public void StartDestroying(int timer){
		var wantedPos = Camera.main.WorldToScreenPoint (this.transform.position);
		var go = (GameObject)Instantiate(_player.ProgressBar, wantedPos, Quaternion.identity);
		go.transform.SetParent(GameObject.Find("Canvas").transform);
		Observable.IntervalFrame(timer).Subscribe(x => {
			wantedPos = Camera.main.WorldToScreenPoint (this.transform.position);
			go.transform.position = wantedPos;
			go.GetComponent<Image>().fillAmount += Time.deltaTime;
		}).AddTo(this);
	    ParentPlane.PlacedCubes.Remove(transform.position);
		Observable.Timer(TimeSpan.FromSeconds(timer)).Subscribe(x => Destroy(go));
		Observable.Timer(TimeSpan.FromSeconds(timer)).Subscribe(x => Destroy(this.transform.gameObject));
	}
}
