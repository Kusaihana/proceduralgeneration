﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public struct Properties
{
	public int Type;
	public bool IsDestroyable;
}

public class WorldGenerator : MonoBehaviour {

	public float SizeX;
	public float SizeZ;
	public float SizeY;

	public List<GameObject> Cubes;
	public Dictionary<Vector3, Properties> PlacedCubes = new Dictionary<Vector3, Properties>();

    public bool FromFile = false;
	public bool FirstGeneration = true;

    public void Start (){
        if (FromFile) return;
        if (FirstGeneration)
        {
            StartCoroutine(SimpleGenerator());
        }
        else
        {
            foreach (var cube in PlacedCubes)
            {
                var newCube = Cubes.ElementAt(cube.Value.Type);
                PlaceBlock(cube.Key, newCube, cube.Value.IsDestroyable, false, cube.Value.Type);
            }
        }
    }

	public void PlaceBlock(Vector3 newPosition, GameObject originalGameobject, bool isDestroyable, bool firstCreation, int type){
		var go = (GameObject)Instantiate(originalGameobject, newPosition, Quaternion.identity);
		go.transform.position = newPosition;
		go.name = "Block@" + go.transform.position;
		go.transform.SetParent(transform);
		go.GetComponent<Block>().ParentPlane = this;
		go.GetComponent<Block>().IsDestroyable = isDestroyable;
		Properties p;
		p.Type = type;
		p.IsDestroyable = isDestroyable;
		if(firstCreation)PlacedCubes.Add(go.transform.position, p);
	}

    private IEnumerator SimpleGenerator()
	{
		uint numberOfInstances = 0;
		const uint instancesPerFrame = 400;

		var limx = this.transform.position.x - SizeX / 2;
		var limz = this.transform.position.z - SizeZ / 2;

		for(var x = limx; x < limx + SizeX; x++)
		{
			for(var z = limz; z < limz + SizeZ; z++)
			{
                //var height = UnityEngine.Random.Range(0, SizeY);
			    var perlin1 = Mathf.PerlinNoise((x + transform.position.x) / SizeX, SizeZ);
			    var perlin2 = Mathf.PerlinNoise((z + transform.position.z) / SizeZ, SizeX);
			    var height = Mathf.FloorToInt(perlin1 * perlin2 * 10);
				for (var y = 0; y <= height; y++)
				{
					// player cannot destroy bottom plane
					var isDestroyable = y > 0;

					var newPosition = new Vector3(x, y, z);
					var type = UnityEngine.Random.Range(0, Cubes.Count());
					var randomlySelectedCube = Cubes.ElementAt(type);
					PlaceBlock(newPosition, randomlySelectedCube, isDestroyable, true, type);

					numberOfInstances++;

				    if (numberOfInstances != instancesPerFrame) continue;
				    numberOfInstances = 0;
				    yield return new WaitForEndOfFrame();
				}
			}
		}
	}
}
