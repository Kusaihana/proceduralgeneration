﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using UniRx;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class LoadManager : MonoBehaviour
{
    [SerializeField]
    public List<Data> DataList = new List<Data>();

    public Data Data;
    public Data[] PrevData;

    private Player _player;

    public void OnNewGameButtonClicked()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _player.CleanWorld();
        _player.NewWorld();

        _player.ButtonsPanel.SetActive(false);
        _player.gameObject.GetComponent<FirstPersonController>().enabled = true;
    }

    public void OnLastGameButtonClicked()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _player.CleanWorld();
        StreamReader reader = new StreamReader(Application.dataPath + "/Resources/savedgame.json");
        var jsonString = reader.ReadToEnd();
        reader.Close();
        PrevData = JsonHelper.FromJson<Data>(jsonString);
        Observable.Timer(TimeSpan.FromSeconds(1)).Subscribe(x =>
        {
            LoadChunks();
            _player.ButtonsPanel.SetActive(false);
            _player.gameObject.GetComponent<FirstPersonController>().enabled = true;

            foreach (var chunk in PrevData)
            {
                if (chunk.Name.StartsWith("Block"))
                {
                    // Generate cubes
                    var parentChunk = GameObject.Find(chunk.ParentName).GetComponentInChildren<WorldGenerator>();
                    var newCube = parentChunk.Cubes.ElementAt(chunk.Type);
                    parentChunk.PlaceBlock(chunk.Position, newCube, chunk.IsDestroyable, true, chunk.Type);
                }
            }

            // destroy the further planes
            _player.VisiblePlanes = _player.VisiblePlanes.OrderBy(p => Vector3.Distance(_player.transform.position, p.transform.position)).ToList();
            var count = _player.VisiblePlanes.Count - 9;
            while (count > 0)
            {
                _player.DestroyLastPlaneFromList();
                count--;
            }
        });
    }

    public void LoadChunks()
    {
        foreach (var chunk in PrevData)
        {
            if (chunk.Name.StartsWith("Plane"))
            {
                // Generate chunks
                _player.CreatePlane(chunk.Position, true);
            }
        }
    }
}